# vim: set tabstop=4
# s_ENGfeatures_regress.py
#!/usr/bin/env python3
""" Engineered features implemented to predict the NH4+-N concentration based on
the oxydation/reduction potential (ORP) signal."""
'''
 Copyright (C) 2018 Juan Pablo Carbajal
 Copyright (C) 2018 Maraine Yvonne Schneider

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
'''

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <m.sammel@meiru.ch>


############
## Imports
# built-ins

# 3rd party
import numpy as np
import matplotlib.pyplot as plt
plt.close('all')
import matplotlib
import platform
if platform.system() is not 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    pass
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'

from sklearn.linear_model import ARDRegression
from sklearn.metrics import mean_squared_error, r2_score

# user
from SBRdata.dataparser import SensorData, IOData
from pH_features import aeration_valley, aeration_climax
from O2_features import respirogram
from ORP_features import aeration_start, aeration_elbow, aeration_plateau, nitrate_knee

# TODO: do this OS robust
datapath = '../data/'
files = {'pH':'AI4_MW.csv', 'O2':'AI2_MW.csv',
        'ORP':'mV ORP CPS12D.1.csv'}
# effconc = {'NH4': '_NH4', 'DOC':'_DOC', 'TOC':'_TOC'}

#' ## Load data and verify integrity of outputs
sensor = dict()
# _NH4 = np.array([])
# _DOC = np.array([])
# _TOC = np.array([])
for signal, filenam in files.items():
    filename = datapath + filenam
    print('Processing signal {} from {}'.format(signal, filename))

    Ydata = IOData(filename)
    _NH4 = Ydata.NH4[:,1]

    # filter NA values in output
    nonan = np.isfinite(_NH4)
    _NH4 = _NH4[nonan]
    # get effluent data for DOC
    _DOC = Ydata.DOC[:,1]
    nonanDOC = np.isfinite(_DOC)
    _DOC = _DOC[nonanDOC]

    _TOC = Ydata.TOC[:,1]
    nonanTOC = np.isfinite(_TOC)
    _TOC = _TOC[nonanTOC]

    # Check allfiles have the same output values
    if not np.allclose (_NH4, Ydata.NH4[nonan,1]):
        raise ValueError('Output signals differ!')

    sensor[signal] = SensorData(filename)

#' ## Filter inputs that do not show pH aeration valley
## Select aeration phase
ORP_aer = np.nonzero(sensor['ORP'].phase==4)[0]
# chooses all pH values which do not have a nonan for NH4 and are in phase 4
_ORP = sensor['ORP'].interp_nan()[nonan,:][:,ORP_aer]
ORP_t = sensor['ORP'].time[ORP_aer]
ORP_t = (ORP_t - ORP_t[0])/(ORP_t[-1] - ORP_t[0])
dt_ORP = ORP_t[1] - ORP_t[0]

ORP = _ORP
NH4 = _NH4

# ORP_M = np.zeros ([ORP.shape[0],2])
# for i,s in enumerate(ORP): # s is all pH values for one cycle
#     kn, s_smooth = knee(ORP_t, s, \
#             smooth_opt=s_climax_opt, findpeaks_opt=fp_climax_opt)
#     ORP_M[i,:] = np.array(kn)
ORP_M = np.zeros ([ORP.shape[0],2])
for i,s in enumerate(ORP):
    max, spl = aeration_start(ORP_t, s)
    ORP_M[i,:] = np.array(max)

ORP_E = np.zeros([ORP.shape[0],2])
for i,s in enumerate(ORP):
    max, spl = aeration_elbow(ORP_t, s)
    ORP_E[i,:] = np.array(max)

# FIXME
slope_tol = 20
window_length = np.minimum(20, len(ORP))
ORP_P = np.zeros([ORP.shape[0],2])
for i,s in enumerate(ORP):
 max, spl = aeration_plateau(ORP_t, s, \
         t_elbow=ORP_E[i,0], slope_tol=slope_tol)
 ORP_P[i,:] = np.array(max)

#' ## Regress with ARD and check the weights
N = ORP.shape[0]
X = np.zeros([N,4])
X[:,0:2] = ORP_M
X[:,2:4] = ORP_E
    
scale = lambda x: (x - np.mean(x,axis=0)) / np.std(x,axis=0) # zscore
#scale = lambda x: (x - x.min(axis=0)) / (x.max(axis=0) - x.min(axis=0)) # baseline
X = scale(X)

## Nonlinear mapping of inputs
# The O2_R zscored  features seem to be have an exponential relation with the output
# e_zs_O2R = scale(np.exp(np.abs(X[:,2:4])))
# #e_zs_O2R = X[:,2:4]
# # plt.ion()
# plt.figure()
# plt.plot(e_zs_O2R, NH4, 'o')
# plt.ylabel('NH4')
# plt.xlabel('exp(X)')
# plt.show()
#
# X = np.concatenate((X, e_zs_O2R), axis=1)

## ARD regression
reg = ARDRegression (n_iter=int(1e3), compute_score=True)
reg.fit(X, NH4)
y_pred = reg.predict(X)

print("Mean squared error: %.2f "
      % mean_squared_error(NH4, y_pred))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f '
      % r2_score(NH4, y_pred))

#+ caption= "ARD feature selection"
plt.ion()
plt.figure()
plt.clf()
_n_ = np.arange(len(reg.coef_))
plt.bar(_n_, reg.coef_)
plt.ylabel('Weight in the model')
name = np.array(['aer_t', 'aer_v','Elb_t', 'Elb_v'])
plt.xticks(_n_, name)
plt.show()

#+ caption= "Regression results."
plt.figure()
plt.clf()
plt.plot(NH4,'o', label='data')
plt.plot(y_pred,'x', label='pred.')
plt.ylabel('NH4')
plt.xlabel('Cycle')
plt.legend()
plt.show()
