# vim: set tabstop=4
# s_publish_examples.py
#!/usr/bin/env python3

#' % Copyright (C) 2018 - Juan Pablo Carbajal
#' % This program is free software; you can redistribute it and/or modify
#' % it under the terms of the GNU General Public License as published by
#' % the Free Software Foundation; either version 3 of the License, or
#' % (at your option) any later version.
#' %
#' % This program is distributed in the hope that it will be useful,
#' % but WITHOUT ANY WARRANTY; without even the implied warranty of
#' % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#' % GNU General Public License for more details.
#' %
#' % You should have received a copy of the GNU General Public License
#' % along with this program. If not, see http://www.gnu.org/licenses/.
#' %
#' % Juan Pablo Carbajal <ajuanpi+dev@gmailcom>
#' % 01.02.2018

import pweave

examples =['s_pcr_phase_pH.py','s_pcr_pH.py','s_ARDfeatures_pH.py', 
           's_ENGfeatures_regress.py']

def main(num=None):
    if not num:
        for ex in examples:
            print ('Publishing %s' % ex)
            pweave.publish (ex)
    else:
        print ('Publishing %s' % examples[num])
        pweave.publish (examples[num])

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
        N = len(examples)
        if n-1 >= N or n <= 0:
            raise ValueError('Invalid example index %d' % n)
        main (num=n-1)
    else:
        main()

