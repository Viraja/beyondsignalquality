# vim: set tabstop=4
# s_fig_TFtale.py
#!/usr/bin/env python3
""" The script creates the contingency tables and plot of the sum of the
true positive and the true negative results obtained when varying the
parameters slope tolerance and cutoff frequency."""

# Copyright (C) 2018 Mariane Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 05.07.2018

############
## Imports
# built-ins
import platform

# 3rd party
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pickle
from scipy.interpolate import griddata

# user
from SBRdata.utils import fscore
############

# matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'Arial'
matplotlib.rcParams.update({'font.size': 12})
# matplotlib.style.use('seaborn')
if platform.system() != 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    pass

if __name__ == '__main__':
    # Sensor type needs to be given when calling function "python s_fig_TFtable
    # DO" and s_O2_ROCcurve called to create the .pkl files.
    import sys
    sensortype = str(sys.argv[1])
    inputfile = '../data/%s_Tables.pkl'%sensortype

    print('Reading contingency tables to %s ...' %inputfile)

    # opens data from .pkl files.
    with open(inputfile, 'rb') as fid:
        TFtable = pickle.load(fid)
        params = pickle.load(fid)
        param_names = pickle.load(fid)

    # Extract table values and reorganize for easy plotting
    COUNT = dict().fromkeys(TFtable.keys())
    vmax = 0
    for sname, table in TFtable.items():
        COUNT[sname] = dict().fromkeys(table[0].keys())
        for etype in COUNT[sname].keys():
            COUNT[sname][etype] = \
                np.array([x[etype] for x in table])
            vmax = np.maximum(vmax, COUNT[sname][etype].max())
    nsens = len(COUNT)
    figs = []
    dim = len(params)
    if dim == 2:
        CMAP = 'inferno_r'
        X0, Y0 = np.meshgrid(params[0], params[1])

        fig = plt.figure(figsize=(10, 5))
        axes = fig.subplots(4, nsens, sharey='all', sharex='all')
        figs.append(fig)

        s = -1
        for sname, count in COUNT.items():
            s += 1
            t = -1
            for K, M in count.items():
                t += 1
                ax = axes[t, s]
                pcm = ax.pcolormesh(X0, Y0, M.reshape(X0.shape), cmap=CMAP,
                                    vmin=0, vmax=vmax)
                ax.set_xlim(1, 5)

                if t == 0:
                    ax.set_title(sname)
                if t == 3:
                    ax.set_xlabel(param_names[0])
                if s == 0:
                    # entry type
                    etype = ''.join([x[0] for x in K.split('_')])
                    ax.set_ylabel('%s\nmin. slope' % etype)
        plt.colorbar(pcm, ax=axes)

        figs[0].savefig('TFtables%s_all.png' % sensortype)

        # Figure with sum of entries
        # Show only TP + TN
        # fig = plt.figure(figsize=(10, 5))
        fig = plt.figure()
        axes = fig.subplots(1, nsens, sharey='all', sharex='all')
        figs.append(fig)

        vmin = [7, 41]
        vmax = [55, 89] # set by hand
        s = -1
        for sname, count in COUNT.items():
            s += 1
            ax = axes[s]
            M = (count['TRUE_POSITIVE'] + \
                 count['TRUE_NEGATIVE']).reshape(X0.shape)
            pcm1 = ax.pcolormesh(X0, Y0, M, cmap=CMAP, vmin=vmin[1],
                                 vmax=vmax[1])
            ax.set_title(sname.replace('_', ' '), fontsize=12)
            ax.set_xlim(1, 5)
            ax.set_xlabel(param_names[0].replace('cutoff', 'cut-off'), fontsize=12)
            if s == 0:
                # entry type
                etype = ''.join([x[0] for x in K.split('_')])
                ax.set_ylabel('minimal slope', fontsize=12)
            print(M.min(), M.max())
        cbar = fig.colorbar(pcm1, ax=axes[:])
        cbar.set_label('sum TP + TN feature detections', rotation=90)
        figs[1].savefig('TFtables%s_sums.png' % sensortype)

        plt.show()

    if dim == 1:
        # Get sequencial colors for lines
        fig = plt.figure(figsize=(10, 5))
        axes = fig.subplots(2, nsens, sharey='row', sharex='all')
        figs.append(fig)

        i = -1
        for sname, count in COUNT.items():
            i += 1
            ax = axes[0, i]
            vmax = 0
            c = -1
            for k, val in count.items():
                c += 1
                etype = ''.join([x[0] for x in k.split('_')])
                ax.plot(params[0], val, label=etype, color='C%d'%c)
                ax.set_title(sname)
                vmax = np.maximum(vmax, val.max())
            ax.set_ylim(-1, vmax+1)
            if i == 0:
                ax.set_ylabel('Count')
            h, l = ax.get_legend_handles_labels()

            ax = axes[1, i]
            valF = count['FALSE_POSITIVE'] + count['FALSE_NEGATIVE']
            valT = count['TRUE_POSITIVE'] + count['TRUE_NEGATIVE']
            ax.plot(params[0], valF, label='Total F', color='C4')
            ax.plot(params[0], valT, label='Total T', color='C5')
            ax.set_ylim(-1, np.maximum(valF.max(), valT.max())+1)

            ax.set_xlim(params[0].min(), 7)
            ax.set_xlabel(param_names[0])
            h_, l_ = ax.get_legend_handles_labels()
            if i == 0:
                ax.set_ylabel('Count')

        fig.legend(h+h_, l+l_, loc="upper center", ncol=4+2)
        fig.tight_layout()

        figs[0].savefig('TFtables%s_all.png' % sensortype)
