'''ys COS61D.3;AI8_MW;Turb CUS52D.1;AI5_MW;mV ORP CPS12D.3;mV ORP CPS12D.1;
DATE;Dev4_RUN;ys O2 COS61D.2;AI1_MW;index;mV CPS11D.1;Temp. CPS11D.1;
Dev3_RUN;mV CPS11D old1;mV CPS11D old2;% ORP CPS12D.3;% ORP CPS12D.2;
% ORP CPS12D.1;pH CPS11D old2;WA_RUN;mV CPS91D.2;Air_RUN;Cycle;O2 COS61D.2;
O2 COS61D.3;AI2_MW;T Turb CUS52D.1;Dev1_RUN;HRT;USS_RUN;Sewer_flow;DATETIME;
pH CPS11D.1;pH CPS91D.2;pH CPS91D.1;AI4_MW;mV CPS91D.1;AI3_MW;
TurbSol CUS51D.1;USS_log;PS_STEP;P01_RUN;Temp. CPS91D.1;pH CPS11D old1;
AI7_MW;O2rate;Turb CUS52D.2;Turb CUS52D.3;TIME;Glasim. CPS11D.1;
mV ORP CPS12D.2;NH4rate;RW_RUN;Dev2_RUN'''

print('start skript')

# -*- coding: utf-8 -*-
# vim: set tabstop=4
# s_figure_binary_nh4_boxplot_subplot.py
#!/usr/bin/env python3
"""Preprocess pH raw data"""

__author__ = "Mariane Yvonne Schneider"
__copyright__ = "Copyright (C) 2018 - Mariane Yvonne Schneider"
__credits__ = ["Mariane Yvonne Schneider"]
__license__ = """This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>."""
__version__ = "0.0.1"
__maintainer__ = "Mariane Schneider"
__email__ = "myschneider@meiru.ch"
__status__ = "Development"

# third party
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from scipy import signal
import platform
import matplotlib
if platform.system() is not 'Windows':
    matplotlib.rcParams['text.usetex'] = True
else:
    pass
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'sans-serif'
matplotlib.rcParams['font.sans-serif'] = 'DejaVu Sans'
matplotlib.rcParams.update({'font.size': 14})
from matplotlib.legend_handler import HandlerLine2D

from sklearn.decomposition import PCA
from scipy.signal import argrelextrema
from datetime import datetime
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText

from SBRdata.dataparser import SensorData, IOData
from pH_features import aeration_valley, aeration_climax

savef = 0               # choose between 1 and 0 depening if the figure should
                        # be saved automatically or not
fontsize = 14           # Larger Fontsize for plots

# ylabel_list = ['pH maintained', 'pH unmaintained']
# ylabel_list = ['O2 [20.05.2017]', 'O2 [02.01.2018]']
#mpl.use('agg')              # to create .png files
datapath = '../data/'
files = {'pH_1':'AI4_MW.csv', 'pH_2':'AI4_MW_all_test.csv'}

''' plots to show differences in identification of ammonium valley between
maintained and unmaintained pH sensors.
'''

#' ## Data load and pre-process
#' First load the data, filter NA values and select the output to regress.
#' In this case we use the NH4 value at the effluent.
sensor = dict()
_NH4 = np.array([])
for signal, filenam in files.items():
    filename = datapath + filenam
    print('Processing signal {} from {}'.format(signal, filename))
    sensor[signal] = SensorData(filename)

Ydata = IOData(datapath + files['pH_1'])
if not _NH4.size > 0:
    _NH4 = Ydata.NH4[:,1]

    # filter NA values in output
    nonan = np.isfinite(_NH4)
    _NH4 = _NH4[nonan]

# Check allfiles have the same output values
if not np.allclose (_NH4, Ydata.NH4[nonan,1]):
    raise ValueError('Output signals differ!')

_dateT_1 = sensor['pH_1'].row_name
dateT_1 = []
for i in range(3, len(_dateT_1)):
    rowname_ = _dateT_1[i]
    datestr = rowname_[:8]
    timestr = rowname_[9:-11]
    dayhour = datestr + timestr
    day = datetime.strptime(datestr, "%Y%m%d").date()
    hour = datetime.strptime(timestr, "%H%M").time()
    dayhour = datetime.combine(day, hour)
    dateT_1.append(dayhour)

_dateT_2 = sensor['pH_2'].row_name
dateT_2 = []
for i in range(3, len(_dateT_2)):
    rowname_ = _dateT_2[i]
    datestr = rowname_[:8]
    timestr = rowname_[9:-11]
    dayhour = datestr + timestr
    day = datetime.strptime(datestr, "%Y%m%d").date()
    hour = datetime.strptime(timestr, "%H%M").time()
    dayhour = datetime.combine(day, hour)
    dateT_2.append(dayhour)
## Filter inputs that do not show pH aeration valley
## Select aeration phase
pH_aer_1 = np.nonzero(sensor['pH_1'].phase==4)[0]
_pH_1 = sensor['pH_1'].interp_nan()[nonan,:][:,pH_aer_1]
pH_t_1 = sensor['pH_1'].time[pH_aer_1]
pH_t_1 = (pH_t_1 - pH_t_1[0])/(pH_t_1[-1] - pH_t_1[0])
dt_1 = pH_t_1[1] - pH_t_1[0]

pH_aer_2 = np.nonzero(sensor['pH_2'].phase==4)[0]
_pH_2 = sensor['pH_2'].interp_nan()[:,pH_aer_2]
pH_t_2 = sensor['pH_2'].time[pH_aer_2]
pH_t_2 = (pH_t_2 - pH_t_2[0])/(pH_t_2[-1] - pH_t_2[0])
dt_2 = pH_t_2[1] - pH_t_2[0]

## Compute amonia valley for pH sensor 1 and 2
fp_valley_opt = {'order':5}
s_valley_opt = {'freq':5}

# Maintained sensor
novalley_1 = np.array ([True]*_pH_1.shape[0])
valley_1 = np.array ([False]*_pH_1.shape[0])
timeVal_python_1 = []
for i,s in enumerate(_pH_1): # s is all pH values for one cycle
    m, s_smooth = aeration_valley(pH_t_1, s, \
                           smooth_opt=s_valley_opt, \
                           findpeaks_opt=fp_valley_opt)
    if m:
        novalley_1[i] = False
        valley_1[i] = True
        timeVal_python_1.append(m)

        # uncomment here to see the signals

print("Number of signals with amonia valley (maintained): {}/{}".format(\
    np.sum(np.logical_not(novalley_1)), _pH_1.shape[0]))

timeVal_1 = np.array(timeVal_python_1)
pH_1 = _pH_1[novalley_1,:]
pHval_1 = _pH_1[valley_1,:]

# Unmaintained sensor
novalley_2 = np.array ([True]*_pH_2.shape[0])
valley_2 = np.array ([False]*_pH_2.shape[0])
timeVal_python_2 = []
novalley_2 = np.array ([True]*_pH_2.shape[0])
for i,s in enumerate(_pH_2): # s is all pH values for one cycle
    m, s_smooth = aeration_valley(pH_t_2, s, \
                           smooth_opt=s_valley_opt, \
                           findpeaks_opt=fp_valley_opt)
    if m:
        novalley_2[i] = False
        valley_2[i] = True
        timeVal_python_2.append(m)

print("Number of signals with amonia valley (unmaintained): {}/{}".format(\
    np.sum(np.logical_not(novalley_2)), _pH_2.shape[0]))

timeVal_2 = np.array(timeVal_python_2)
pH_2 = _pH_2[novalley_2,:]
pHval_2 = _pH_2[valley_2,:]

#' ## PCR training
#' We extract PCs from the whole dataset
pca_1 = PCA(n_components=7, svd_solver='full')
pca_2 = PCA(n_components=7, svd_solver='full')
pca_1.fit(pH_1)
pca_2.fit(pH_2)


print('Number of Principal components for reference sensors: %.2f'
      % pca_1.n_components_)
print('Number of Principal components for reference sensors: %.2f'
      % pca_2.n_components_)

timeno_val_1 = [140]*len(pH_1)
timeno_val_2 = [140]*len(pH_2)
timepHlist_1 = timeVal_1[:,0]*100
timepHlist_2 = timeVal_2[:,0]*100
threashold = 3
# above_threashold_1 = NH4val_1[NH4val_1 > threashold]
# above_1 = timepHlist_1[NH4val_1 > threashold]
# below_threashold_1 = NH4val_1[NH4val_1 < threashold]
# below_1 = timepHlist_1[NH4val_1 < threashold]
#
# above_threashold_2 = NH4val_2[NH4val_2 > threashold]
# above_2 = timepHlist_2[NH4val_2 > threashold]
# below_threashold_2 = NH4val_2[NH4val_2 < threashold]
# below_2 = timepHlist_2[NH4val_2 < threashold]

fig_1, ax_1 = plt.subplots(nrows=1, ncols=1)
# line1, = ax_1.plot(above_1,above_threashold_1, 'ro', markersize=8, label = '> 3 $mg_{NH_4-N}/L$ gewartet')
# line2, = ax_1.plot(below_1,below_threashold_1, 'go', markersize=8, label = '< 3 $mg_{NH_4-N}/L$ gewartet')
# for i in range(len(timeno_val_1)):
#     if NH4_1[i] < 3:
#         ax_1.plot(timeno_val_1[i],NH4_1[i], 'go', markersize=8, label = '_nolegend_')
#     if NH4_1[i] >= 3:
#         ax_1.plot(timeno_val_1[i],NH4_1[i], 'ro', markersize=8)
#
# line3, = ax_1.plot(above_2,above_threashold_2, 'rx', linewidth=4, markersize=10, label = '> 3 $mg_{NH_4-N}/L$ ungewartet')
# line4, = ax_1.plot(below_2,below_threashold_2, 'gx', linewidth=4, markersize=10, label = '< 3 $mg_{NH_4-N}/L$ ungewartet')
# for i in range(len(timeno_val_2)):
#     if NH4_2[i] < 3:
#         ax_1.plot(timeno_val_2[i],NH4_2[i], 'gx', linewidth=4, markersize=10, label = '_nolegend_')
#     if NH4_2[i] >= 3:
#         ax_1.plot(timeno_val_2[i],NH4_2[i], 'rx', linewidth=4, markersize=10)

ax_1.set_ylabel('Binary Signal')
ax_1.set_xlabel( 'cycle number')
for i in range(len(_NH4)):
    if _NH4[i] < 0.6:
        ax_1.plot(dateT_1[i],valley_1[i], 'go', linewidth=4, markersize=7)
    if _NH4[i] >= 0.6:
        ax_1.plot(dateT_1[i],valley_1[i], 'ro', linewidth=4, markersize=7)

ax_1.legend(loc=2, ncol=1)
# ax_1.set_ylim(0,50)
# ax_1.set_xlim(0,145)
# ax_1.set_xticklabels(labels)
# labels = [ '\'valley\'','kein \'valley\'']
labels = ['', 'False' ,'True']
ax_1.set_yticklabels(labels)
plt.tight_layout()
if savef == 1:
    fig_1.savefig('../figures/timeline_all_un-maintained.png')
    fig_1.savefig('../figures/timeline_all_un-maintained.eps', format='eps', dpi=1000)
else:
    pass

plt.show()

fig_2, ax_2 = plt.subplots(nrows=1, ncols=1)
# line1, = ax_2.plot(above_1,above_threashold_1, 'ro', markersize=8, label = '> 3 $mg_{NH_4-N}/L$ gewartet')
# line2, = ax_2.plot(below_1,below_threashold_1, 'go', markersize=8, label = '< 3 $mg_{NH_4-N}/L$ gewartet')
# for i in range(len(timeno_val_1)):
#     if NH4_1[i] < 3:
#         ax_2.plot(timeno_val_1[i],NH4_1[i], 'go', markersize=8, label = '_nolegend_')
#     if NH4_1[i] >= 3:
#         ax_2.plot(timeno_val_1[i],NH4_1[i], 'ro', markersize=8)
#
# line3, = ax_2.plot(above_2,above_threashold_2, 'rx', linewidth=4, markersize=10, label = '> 3 $mg_{NH_4-N}/L$ ungewartet')
# line4, = ax_2.plot(below_2,below_threashold_2, 'gx', linewidth=4, markersize=10, label = '< 3 $mg_{NH_4-N}/L$ ungewartet')
# for i in range(len(timeno_val_2)):
#     if NH4_2[i] < 3:
#         ax_2.plot(timeno_val_2[i],NH4_2[i], 'gx', linewidth=4, markersize=10, label = '_nolegend_')
#     if NH4_2[i] >= 3:
#         ax_2.plot(timeno_val_2[i],NH4_2[i], 'rx', linewidth=4, markersize=10)

ax_2.set_ylabel('Binary Signal')
ax_2.set_xlabel( 'cycle number')
ax_2.plot(dateT_2,valley_2, 'bo', linewidth=4, markersize=7)
ax_2.legend(loc=2, ncol=1)
# ax_2.set_ylim(0,50)
# ax_2.set_xlim(0,145)
# ax_2.set_xticklabels(labels)
# labels = [ '\'valley\'','kein \'valley\'']
labels = ['', 'False' ,'True']
ax_2.set_yticklabels(labels)
plt.tight_layout()
if savef == 1:
    fig_1.savefig('../figures/timeline_all_un-maintained.png')
    fig_1.savefig('../figures/timeline_all_un-maintained.eps', format='eps', dpi=1000)
else:
    pass

plt.show()

fig_3, ax_3 = plt.subplots(nrows=1, ncols=1)
ax_3.set_ylabel('Binary Signal')
ax_3.set_xlabel( 'cycle number')
ax_3.plot(dateT_2,valley_2, 'bo', linewidth=4, markersize=7)
for i in range(len(_NH4)):
    if _NH4[i] < 0.6:
        ax_3.plot(dateT_1[i],valley_1[i], 'go', linewidth=4, markersize=7)
    if _NH4[i] >= 0.6:
        ax_3.plot(dateT_1[i],valley_1[i], 'ro', linewidth=4, markersize=7)

ax_3.legend(loc=2, ncol=1)
labels = ['', 'False' ,'True']
ax_3.set_yticklabels(labels)
plt.tight_layout()
if savef == 1:
    fig_3.savefig('../figures/timeline_all_combined.png')
    fig_3.savefig('../figures/timeline_all_combined.eps', format='eps', dpi=1000)
else:
    pass

plt.show()
