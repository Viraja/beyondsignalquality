# vim: set tabstop=4
# s_featuredemo_figure.py
#!/usr/bin/env python3
""" The script shows the implemented features on a synthetic signals. """

# Copyright (C) 2018 Mariane Yvonne Schneider
# Copyright (C) 2018 Juan Pablo Carbajal
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 03.10.2018

import numpy as np

import matplotlib
import matplotlib.pyplot as plt

from functools import partial

from SBRfeatures.pH_features import aeration_valley
from SBRfeatures.O2_features import aeration_ramp
from SBRfeatures.ORP_features import aeration_elbow
from SBRfeatures.basic_funcs import smooth

def plot2dfeature (t, vals):
    point = plt.plot (t, vals[0], 'o')
    xlin = t + np.array([-0.05, 0.05])
    ylin = vals[0] + vals[1] * (xlin - t)
    line = plt.plot (xlin, ylin, '-')
    line[0].set_c(point[0].get_c())
    return point[0], line[0]

if __name__ == '__main__':

    # Generate fake signal
    t = np.linspace(0, 1, 250)
    s = np.exp(-t / 0.1) + (np.tanh((t - 0.3) / 0.1) + 1) / 2 \
        + (np.tanh((t - 0.8) / 0.05) + 1) / 2 \
        + 0.2 * np.sin ( 2 * np.pi * 10 * t)
    sn = s + 0.05 * np.random.randn(*s.shape)

    # Smoothing function for all features
    nyqf = (1 / np.diff(t).min() / 2) # Nyquist freq.
    cut_freq = 0.02 * nyqf
    smoothfun = partial(smooth, order=3, freq=cut_freq)

    # pH: aeration valley
    t_valley, val_valley, ss = aeration_valley(t, sn, smoother=smoothfun)
    # O2: aeartion ramp
    t_ramp, val_ramp, _ = aeration_ramp(t, sn, smoother=smoothfun)
    # ORP: aeration elbow
    t_elbow, val_elbow, _ = aeration_elbow(t, sn, smoother=smoothfun, \
        t_aeration=t_valley+0.2)

    matplotlib.style.use('seaborn')
    # plt.ion()
    plt.figure()
    plt.plot(t, sn, '.k', label='signal')
    plt.plot(t, ss, label='smoothed')

    vh = plt.plot(t_valley, val_valley, 'o')
    rh, _ = plot2dfeature(t_ramp, val_ramp)
    eh, _ = plot2dfeature(t_elbow, val_elbow)

    # Annotations
    valley_color = vh[0].get_c()
    bbox_props = dict(boxstyle="rarrow,pad=0.3", \
        fc=valley_color, ec=valley_color, lw=2, alpha=0.8)
    th = plt.text(t_valley-0.02, val_valley-0.1, "valley", ha="right", va="top", \
            rotation=45, size=15, bbox=bbox_props)
    bbox_props['fc'] = bbox_props['ec'] = rh.get_c()
    th = plt.text(t_ramp-0.02, val_ramp[0]+0.1, "ramp", ha="right", va="bottom", \
            rotation=-45, size=15, bbox=bbox_props)

    plt.legend()
    plt.xlabel('time [arbitrary unit]')
    plt.ylabel('signal [arbitrary unit]')
