# vim: set tabstop=4
# sensorlist.py
#!/usr/bin/env python3
""" List for all testsensors in SBR Bettina."""

############
## Imports
# built-ins

# 3rd party
import numpy as np
from scipy import signal
from collections import namedtuple

# user
############

__author__ = "Juan Pablo Carbajal, Mariane Yvonne Schneider"
__copyright__ = "Copyright (C) 2018 - Juan Pablo Carbajal, Maraine Yvonne Schneider"
__credits__ = ["Juan Pablo Carbajal", "Mariane Yvonne Schneider"]
__license__ = """This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>."""
__version__ = "0.0.1"
__maintainer__ = "Juan Pablo Carbajal", "Mariane Yvonne Schneider"
__email__ = "ajuanpi+dev@gmailcom", "m.sammel@meiru.ch"
__status__ = "Development"

'''Possible Variables:
ORP: mV ORP CPS12D.1-3
DO: AI2_MW, O2 COS61D.2, O2 COS61D.3
pH: pH CPS11D.1, pH CPS91D.1-2, AI3_MW, AI4_MW - AI4_MW reference sensor
Turbidity: Turb CUS52D.1-3, TurbSol CUS51D.1
Conductivity: AI5_MW
Ammonium: AI7_MW
Nitrate: AI8_MW
Aeration on or off: Air_RUN
'''

files = namedtuple('files',["maintained", "type", "file"])

pH01 = files(maintained = True, type = "pH", file = "AI4_MW")
pH02 = files(maintained = False, type = "pH", file = "pH CPS11D.1")
pH03 = files(maintained = False, type = "pH", file = "AI3_MW")
pH04 = files(maintained = False, type = "pH", file = "pH CPS91D.1")
pH05 = files(maintained = False, type = "pH", file = "pH CPS91D.2")
orp01 = files(maintained = True, type = "ORP", file = "mV ORP CPS12D.1")
orp02 = files(maintained = False, type = "ORP", file = "mV ORP CPS12D.2")
orp03 = files(maintained = False, type = "ORP", file = "mV ORP CPS12D.3")
do01 = files(maintained = True, type = "DO", file = "AI2_MW")
do02 = files(maintained = False, type = "DO", file = "O2 COS61D.2")
do03 = files(maintained = False, type = "DO", file = "O2 COS61D.3")
# Turbidity_1 = files(maintained = True, type = "Turb", file = "Turb CUS52D.1")
# Turbidity_2 = files(maintained = False, type = "Turb", file = "Turb CUS52D.2")
# Turbidity_3 = files(maintained = False, type = "Turb", file = "Turb CUS52D.3")
# "Air_RUN": files(file = "Air_RUN")
# "Sensor_Cleaning": files(file = "Dev1_RUN")
# FIXME where do we put input data as the air run? It is not
# maintained or not.
