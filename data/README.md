# Data folder

This folder is for the raw data used to create the features for regression.
Please do not add raw data to the repository.

Data created by the scripts in the repository are stored here as well.
After you run some of the scripts you will get some files here.
Please do not add them to the repository.

The full raw dataset will be shared in th enear future eithe rin Zenodo or OSF.

## Instructions to use the data

Comming soon...

